package com.skylabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgroMarketPlaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgroMarketPlaceApplication.class, args);
	}
}
